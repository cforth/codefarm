codefarm--编程练习
=====================

######作者:十八哥
######E-mail:cforth@163.com
---------------------


###anagram
字谜游戏，找出一本字典里所有的变位词。


###game
C语言写的小游戏。


###hanoi_game
汉诺塔游戏，包含了linux和windows平台版本。


###js
javascript练习。


###k_r
K&R练习题代码。


###lottery
彩票游戏，目前有刮刮乐和排列三彩票游戏。


###lua
学习lua的练习代码库。


###morse_code
莫尔斯电码转换。


###other
其他杂七杂八代码。


###puzzle_game
重排九宫系列游戏。


###random_game
随机游走系列游戏。


###ruby
Ruby练习。


###scheme
SICP练习题代码。


###sh
bash学习代码库。


###jQueryTest
jQuery学习代码。


###nodejs
nodejs学习代码。
网址：[**Node入门**](http://www.nodebeginner.org/index-zh-cn.html)


###vimrc
windows下使用vim的配置
